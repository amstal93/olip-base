#!/opt/venv/bin/python3
# -*- coding: utf-8 -*-

import persistqueue
import subprocess
from pid import PidFile


class QueueHandler():

    def __init__(self):
        self.q = persistqueue.SQLiteQueue('/data/content/queue')

    def get_message(self):
        if self.q.size > 0:

            print(f'Size: {self.q.size}')
            res = self.q.get()

            if "installed" in res:
                print(f'Install content with content_id {res.get("installed")[0]} and filename {res.get("installed")[1]}')

                result = subprocess.run('/usr/local/bin/install_content.sh ' + res.get("installed")[0] + ' ' + res.get("installed")[1], shell=True, universal_newlines=True, check=True)

                if result.returncode != 0:
                    print("[+] ERROR {}.".format(result.stderr))
                    sys.exit(0)

                self.q.task_done()

            elif "updated" in res:
                print(f'Upgrade content with content_id {res.get("upgraded")[0]} and filename {res.get("upgraded")[1]}')

                result = subprocess.run('/usr/local/bin/update_content.sh ' + res.get("upgraded")[0] + ' ' + res.get("upgraded")[1], shell=True, universal_newlines=True, check=True)

                if result.returncode != 0:
                    print("[+] ERROR {}.".format(result.stderr))
                    sys.exit(0)

                self.q.task_done()

            elif "uninstalled" in res:
                print(f'Uninstall content with content_id {res.get("uninstalled")[0]} and filename {res.get("uninstalled")[1]}')

                result = subprocess.run('/usr/local/bin/uninstall_content.sh ' + res.get("uninstalled")[0] + ' ' + res.get("uninstalled")[1], shell=True, universal_newlines=True, check=True)

                if result.returncode != 0:
                    print("[+] ERROR {}.".format(result.stderr))
                    sys.exit(0)

                self.q.task_done()

            else:
                print('Queue error')
                del self.q
        else:
            print('Queue is empty, exit')


if __name__ == "__main__":

    with PidFile('queue.pid', piddir="/var/run/") as p:
        olip = QueueHandler()
        olip.get_message()
