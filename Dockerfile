FROM --platform=$TARGETPLATFORM ubuntu:bionic-20210416

ENV DEBIAN_FRONTEND noninteractive
# Do not cache apt packages
# https://wiki.ubuntu.com/ReducingDiskFootprint
RUN echo 'Acquire::http {No-Cache=True;};' > /etc/apt/apt.conf.d/no-cache && \
    echo 'APT::Install-Recommends "0"; APT::Install-Suggests "0";' > /etc/apt/apt.conf.d/01norecommend

SHELL ["/bin/bash", "-c"]

# hadolint ignore=DL3008,DL3015
RUN apt-get -y update && \
    # Software installation (for add-apt-repository and apt-key)
    apt-get -y install \
        ca-certificates \
        cron \
        curl \
        dirmngr \
        dnsutils \
        file \
        git \
        iputils-ping \
        less \
        locales \
        locales-all \
        lsof \
        netcat \
        net-tools \
        openssl \
        python3-pip \
        python3-setuptools \
        python3-venv \
        sqlite3 \
        stress \
        sudo \
        supervisor \
        telnet \
        tree \
        unzip \
        uuid \
        vim \
        wget \
        zip \
    # Delete apt-cache and let people apt-update on start. Without this, we keep getting apt errors for --fix-missing
    && rm -rf /var/cache/apt /var/lib/apt/lists

# hadolint ignore=SC1091
RUN python3 -m venv /opt/venv && \
    . /opt/venv/bin/activate && \
    /opt/venv/bin/python3 -m pip install --upgrade pip && \
    /opt/venv/bin/python3 -m pip install pid persistqueue

# disable editor features which don't work with read-only fs
RUN echo "set noswapfile" >> /etc/vim/vimrc.local && \
    echo "unset historylog" >> /etc/nanorc

# this also sets /etc/default/locale (see detailed notes in README)
RUN update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 LC_ALL=en_US.UTF-8

# Copy OLIP Queue management script
COPY scripts/* /usr/local/bin/
